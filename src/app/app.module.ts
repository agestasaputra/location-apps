import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LocationListingComponent } from './location-listing/location-listing.component';
import { LocationDetailComponent } from './location-detail/location-detail.component';

import { LocationService } from './location.service'
import { DatalistService } from './datalist.service'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'

//import additional package
import { Ng2SearchPipeModule } from 'ng2-search-filter'
// import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LocationListingComponent,
    LocationDetailComponent,
    // NgxPaginationModule
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    HttpClientModule,
    FormsModule,
    Ng2SearchPipeModule
  ],
  providers: [LocationService, DatalistService],
  bootstrap: [AppComponent]
})
export class AppModule { }
