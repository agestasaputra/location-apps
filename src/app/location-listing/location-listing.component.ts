import { Component, OnInit } from '@angular/core';
import { DatalistService } from '../datalist.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-location-listing',
  templateUrl: './location-listing.component.html',
  styleUrls: ['./location-listing.component.css']
})
export class LocationListingComponent implements OnInit {

  public dataLists: any = []
  public searchText: any = ''

  constructor(private _datalistService: DatalistService, private router: Router) { }

  async ngOnInit() {
    // this.location = this._locationService.getLocation()
    await this._datalistService.getDataList()
    .subscribe(data => this.dataLists = data)
    // .subscribe(data => console.log('cek data:', data))
    console.log('cek search:', this.searchText)
  }

  // onSelect = (data) => {
  //   console.log('cek data: ', data)
  //   this.router.navigate(['/location', data.id])
  // } 
}
