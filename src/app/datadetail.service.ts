import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { IDatadetail } from './datadetail'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})

export class DatadetailService {

  constructor(private http: HttpClient) {}

  getDataDetail = (id) : Observable<IDatadetail[]> => {
    return this.http.get<IDatadetail[]>(`http://52.76.85.10/test/profile/${id}.json`)
  }
}