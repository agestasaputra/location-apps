import { Component, OnInit } from '@angular/core';
import { LocationService } from '../location.service'


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public locations: any = []
  public searchText = ""
  public customId = "This is ID"
  public page = 1;

  constructor(private _locationService: LocationService ) { }

  onPageChange = (value) => {
    console.log('on Page Changed!')
    console.log('cek value: ', value)
    this.page = value
  }

  // onSearch = () => {
  //   search
  // }

  async ngOnInit() {
    // this.location = this._locationService.getLocation()
    await this._locationService.getLocation()
    .subscribe(data => this.locations = data)
    // .subscribe(data => console.log('cek data:', data))
    console.log('cek search:', this.searchText)
  }

}
