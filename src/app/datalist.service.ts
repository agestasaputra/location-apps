import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { IDataList } from './datalist'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class DatalistService {

  constructor(private http: HttpClient) { }
  
  getDataList = () : Observable<IDataList[]> => {
    return this.http.get<IDataList[]>('http://52.76.85.10/test/datalist.json')
  }
}
