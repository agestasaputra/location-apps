import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { ILocation } from './location'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) { }

  // getLocation = () => {
  //   return [
  //     {"id": 1, "name": "Anna"},
  //     {"id": 2, "name": "Goks"},
  //     {"id": 3, "name": "Sekali"}
  //   ]
  // }

  getLocation = () : Observable<ILocation[]> => {
    return this.http.get<ILocation[]>('http://52.76.85.10/test/location.json')
  }
}
