export interface IDatadetail {
    id: number,
    name: string,
    speciality: string,
    area: string,
    currency: string,
    rate: number,
    recommendation: number,
    schedule: string,
    experience: number,
    latitude: number,
    longitute: number,
    photo: string,
    description: number
}
