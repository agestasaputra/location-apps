export interface IDataList {
    id: number,
    name: string,
    speciality: string,
    area: string,
    currency: string,
    rate: number,
    photo: string,
}