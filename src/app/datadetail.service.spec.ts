import { TestBed } from '@angular/core/testing';

import { DatadetailService } from './datadetail.service';

describe('DatadetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatadetailService = TestBed.get(DatadetailService);
    expect(service).toBeTruthy();
  });
});
