import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { DatadetailService } from '../datadetail.service'

@Component({
  selector: 'app-location-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.css']
})
export class LocationDetailComponent implements OnInit {

  public dataDetail : any = []
  public id = ''

  constructor(private route: ActivatedRoute, private _dataDetailService: DatadetailService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id')
    console.log('cek id: ', this.id)  

    this._dataDetailService.getDataDetail(this.id)
    .subscribe(data => console.log('cek data in location-detail: ', data))
    // .subscribe(data => this.dataDetail = data)
  }

}
