# LocationApps

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## How to Run this Project

- Yarn install/npm install (install dependencies)
- Yarn start/npm start/ng serve -o (start the projects)
- Yarn build/npm build